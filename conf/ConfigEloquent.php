<?php

namespace conf;
use Illuminate\Database\Capsule\Manager as DB;

class ConfigEloquent{
	
	public static $db=null;
	
	public static function configElo($config_file) {
		self::$db = new DB();
		$tab = parse_ini_file ( $config_file );
		self::$db->addConnection ( $tab );
		self::$db->setAsGlobal ();
		self::$db->bootEloquent ();
	}
}