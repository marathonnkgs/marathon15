<?php
namespace app\control;

use picof\AbstractController as ac;
use app\model\Items;
use app\model\Pieces;
use app\model\Types;
use app\vue\VueAffichage;

/**
 * Classe permettant de controller l'affichage des billets et catégories
 *
 */
class AfficheControl extends ac{

	public function __construct($httprequest){
		parent::__construct($httprequest);
	}

	/**
	 * controle l'accueil de la page
	 * @return string
	 */
	public function accueilControl(){
		$vue = new VueAffichage();
		return $vue->accueil();
	}
	
	
	public function listerItems(){
		$listItem = Items::all() ;
		$va = new VueAffichage($listItem);
		return $va->render(2);
	}
	
	
	public function afficheItem(){
		if (isset ( $this->requete->get ['id'] )) {
			$id = $this->requete->get ['id'];
			$items = Items::find($id);
			if (isset ( $items )) {
				$vb = new VueAffichage ( array ($items ) );
				return $vb->render(1);
			} else {
				echo 'Billet inexistant';
			}
		}
	}
	
	/**
	 * Methode pour le bloc catégorie
	 */
	/*public function listePrefere(){
		$listeitem;
		for($i=1; $i<=5; $i++){
			$max=0;
			$bool=false;
			$item2=null;
			foreach ( $this->tabItem as $item ) {
				foreach ( $this->Listeitem as $item2 ) {
					if($item->id != $item2->id){
						if($item->NbJaime>$item2->NbJaime){
							$item2=$item;
							$bool=true;
							$max=$item->NbJaime;
						}
					}
				}
				if($bool){
					$listeitem.add($item2);
				}
			}
		}
		return $listeitem;
	}*/
	
	/**
	 * Methode liste par couleur
	 */
	public function listerItemsCouleur(){
		$listeItem = Items::all() ;
		if(isset($this->requete->get['couleur'])){
			$listeCouleur;
			foreach ($this->$listeItem as $item ){
				if($item->couleur == $this->requete->get['couleur']){
					array_push($listeCouleur, $item);
				}
			}
			$va=new VueAffichage($listeCouleur);
			return $va->render(2);
		}
	}
	
	
	
	public function listerItemsType(){
		$listeItem = Items::all();
		if(isset($this->requete->get['idtype'])){
			$type = Types::find($this->requete->get['idtype']);
			$listeitems=$type->items();
			$va=new VueAffichage($listeitems);
			return $va->render(2);
		}
	}
	
	
	public function listerItemsPiece(){
		$ListeItem = Items::all();
		if(isset($this->httpRequest->get['idpiece'])){
			$type = Types::find('idpiece');
			$listeitems=$piece->items();
			$va=new VueAffichage($Listeitems);
			return $va->render(2);
		}
	}
	

}