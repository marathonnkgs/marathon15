<?php
namespace app\model;

use \Illuminate\Database\Eloquent\Model as elo;

class Pieces extends elo{

	protected $table = 'ccd_pieces';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function items(){
		return $this->hasMany('blogapp\model\Items', 'piece_id');
	}

}