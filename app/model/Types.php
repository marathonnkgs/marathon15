<?php
namespace app\model;

use \Illuminate\Database\Eloquent\Model as elo;

class Types extends elo{

	protected $table = 'cdd_types';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function items(){
		return $this->hasMany('blogapp\model\Items', 'type_id');
	}


}