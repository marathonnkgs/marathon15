<?php
namespace app\model;

use \Illuminate\Database\Eloquent\Model as elo;

class Items extends elo{

	protected $table = 'ccd_items';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function type(){
		return $this->belongsTo('blogapp\model\Types', 'type_id');
	}
	
	public function piece(){
		return $this->belongsTo('blogapp\model\Pieces', 'piece_id');
	}

}