<?php

namespace app\vue;

use app\model\Items;
use app\model\Types;
use app\model\Pieces;

/**
 * Classe repr�sentant la partie vue du site
 *
 */
class VueAffichage {
	protected $tabBillet, $mess_err;
	public function __construct($tabL = null, $mess = null) {
		$this->tabItem = $tabL;
		$this->mess_err = $mess;
	}
	
	/**
	 * Methode affichant la page d'accueil
	 *
	 * @return retourne la page html de l'accueil
	 */
	public function accueil() {
		$res = '<!DOCTYPE html>
		<html lang="fr">
			<head>
				<meta charset="UTF-8!">
				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
					<link rel="stylesheet" media="screen" href="..\css\index.css" />
				<title>Maison Azur</title>
			</head>
			<header>
				<div class= "logo"><a href="../site/accueil"><img src="../img/blog.png" alt="Logo" title="Logo"/></a></div>
				<div class="titre"><h1>Maison Azur</h1></div>
			</header>
			<body>
				
			<nav>
				<ul id="menu">
					<li class="menu"><a href="accueil">Accueil</a></li>
				
						<li>
							<a href="listItem">Catalogues</a>
						</li>
				</ul>
			</nav>
				<section>';
		$res .= $this->blocCategorie ();
		$res .= '<div id="accueil"> <h2> Bienvenue sur le site de Maison Azur ! </div>';
		$res .= $this->blocBilletsRecents ();
		$res .= '</section>
				
			<script src="../js/jquery.js"></script>
    		<script src="../js/menuJs.js"></script>
			</body>
			<footer>
				<p>�crit en PHP MYSQL HTML/CSS</p>
			</footer>
		</html>';
		return $res;
	}
	
	/**
	 * Soit un affichage d�taill� d'un billet (1)
	 * Soit un affichage d'une liste de billets (2)
	 *
	 * @param int $sel        	
	 */
	public function render($sel) {
		$res = '<!DOCTYPE html>
				<html lang="fr">
				<head>
					<meta charset="UTF-8!">
				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			<link rel="stylesheet" media="screen" href="..\css\index.css" />
					<title>Maison Azur</title>
				</head>
				<header>
					<div class= "logo"><a href="../site/accueil"><img src="../img/blog.png" alt="Logo" title="Logo"/></a></div>
					<div class="titre"><h1>Maison Azur</h1></div>
				</header>
				<body>
				<nav>
				<ul id="menu">
					<li class="menu"><a href="accueil">Accueil</a></li>
				
						<li class="menu">
							<p class="sub">Pieces</p>
						<ul>
	                  		<li><a href="#">Salon</a></li>
	                   		<li><a href="#">Salle � manger</a></li>
	                    	<li><a href="#">Chambre � coucher</a></li>
							<li><a href="#">Salle de bain</a></li>
	                	</ul>
				
						<li class="menu">
							<p class="sub">Types</p>
							<ul>
	                  		<li><a href="#">Salon</a></li>
	                   		<li><a href="#">Salle � manger</a></li>
	                    	<li><a href="#">Chambre � coucher</a></li>
							<li><a href="#">Salle de bain</a></li>
	                		</ul>
						</li>
				
						<li class="menu">
							<p class="sub">Couleur</p>
							<ul>
	                  		<li><a href="/site/item?couleur="Mauve">Mauve</a></li>
	                   		<li><a href="#">Marron</a></li>
	                    	<li><a href="#">Orange</a></li>
							<li><a href="#">Rouge</a></li>
							<li><a href="#">Brun/a></li>
							<li><a href="#">Jaune</a></li>
							<li><a href="#">Blanche</a></li>
	                	</ul>
						</li>
				
						<li>
							<a href="listItem">Catalogues</a>
						</li>
				</ul>
			</nav>
				<section>' . $this->blocCategorie ();
		switch ($sel) {
			case 1 :
				$res .= $this->afficheItem();
				break;
			case 2 :
				$res .= $this->afficheListeItem ();
				break;
			default :
				$res .= "<h2>ERREUR DE PAGE</h2>";
				break;
		}
		$res .= $this->blocBilletsRecents ();
		$res .= '</section>
				<script src="../js/jquery.js"></script>
    		<script src="../js/menuJs.js"></script>
				</body>
		<footer>
		<p>�crit en PHP MySQL HTML/CSS</p>
		</footer>
		</html>';
		return $res;
	}
	
	/**
	 * Methode pour generer le bloc de cat�gorie
	 * construit un fragment html (<aside> .
	 *
	 *
	 * . </aside>)
	 */
	private function blocCategorie() {
		$res = '<nav id="listcat">
				<h2 id="TitreCat"> Liste des cat�gorie </h2>
		</nav>';
		return $res;
	}
	
	/**
	 * Methode g�n�rant fragment html correspondant au bloc
	 * des 10 billets les plus r�cent
	 */
	private function blocBilletsRecents() {
		$res = '<aside id="recent10">
			<h2> Vos articles pr�f�r�s</h2>
		</aside>';
		return $res;
	}
	
	/**
	 * Methode affichant le detail d'1 billet
	 * en fragment html (<div> .
	 *
	 * . <div/>)
	 * utiliser dans affichGeneral
	 */
	public function afficheItem() {
			$res= '<div id="indiv">'.
			$this->tabItem[0]->nom.
			'<p> </br></br>'.$this->tabItem[0]->description.'</p>
					
			Num�ro piece : '.$this->tabItem[0]->piece_id.
			"</br> 	<img src='..\img\\".$this->tabItem[0]->photo.'\' alt="Photo de l\'objet" />'.
			
			'</br> Couleur : '.$this->tabItem[0]->couleur.'<br>'.$this->tabItem[0]->prix.
			'</br> Type id : '.$this->tabItem[0]->type_id.'<br>
			</div>';
			return $res;
	}
	
	/**
	 * Methode affichant une liste de billet
	 * en fragment html (<div> .
	 *
	 * . <div/>)
	 * utiliser dans render
	 */
	public function afficheListeItem() {
		$res = '<div id="listBillet">';
		
		foreach ( $this->tabItem as $item ) {
			
			$res.= "</br> <img src='..\img\\".$item->photo.'\' alt="Photo de l\'objet" />  
				
			<a href="../site/item?id='.$item->id.'">'.$item->nom.'</a>';
				
			$res.= $item->prix.' euro </p>';
		}
		
		$res.="</div>";
		return $res;
	}
}
