$(function(){

	var jq1 = { modules : {}};
	
	/**
	 * module pour l'exercice 4
	 * permet d'afficher les sous menu
	 */
	jq1.modules.menu = (function(){
		
		return{
			piece : function(){
				//selectionne la balise suivante qui est le sous menu et l'affiche/le cache 
				$($("p.sub:contains(Pieces)")).next().toggle("slow");
				$($("p.sub:contains(Couleur)")).next().hide("slow");
				$($("p.sub:contains(Types)")).next().hide("slow");
			},

			couleur : function(){
				$($("p.sub:contains(Pieces)")).next().hide("slow");
				$($("p.sub:contains(Couleur)")).next().toggle("slow");
				$($("p.sub:contains(Types)")).next().hide("slow");
			},

			type : function(){
				$($("p.sub:contains(Pieces)")).next().hide("slow");
				$($("p.sub:contains(Couleur)")).next().hide("slow");
				$($("p.sub:contains(Types)")).next().toggle("slow");
			},

			init : function(){
				$("p.sub:contains(Pieces)").click(this.piece);
				$("p.sub:contains(Couleur)").click(this.couleur);
				$("p.sub:contains(Types)").click(this.type);
			}
		}
	})();
	
	jq1.modules.menu.init();
	
});