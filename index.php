<?php
require_once 'vendor/autoload.php';

$app = new picof\dispatch\Dispatcher ( new picof\utils\HttpRequest());

// blog
$app->addRoute ( '/site/accueil', '\app\control\AfficheControl' , 'accueilControl' );
$app->addRoute ( '/site/item', '\app\control\AfficheControl', 'afficheItem' );
$app->addRoute ( '/site/listItem', '\app\control\AfficheControl', 'listerItems' );

//$app->addRoute ( '/blog/listAut', '\app\control\BlogController', 'listerBilletAuteur' );

conf\ConfigEloquent::configElo ('marathon.config.ini');
session_start();
echo $app->dispatch();


