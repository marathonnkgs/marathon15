<?php

namespace picof;

abstract class AbstractController{

	protected $requete;

	public function __construct($httprequest){
		$this->requete=$httprequest;
	}



	public function __get($attname){
		if(property_exists($this, $attname)){
			return $this->$attname;
		} else {
			throw new \Exception("invalid property");
		}
	}

	public function __set($attname, $attrval){
		if(property_exists($this, $attname)){
			$this->$attname=$attrval;
			return $this->$attname;
		} else throw new \Exception("invalid property");
	}

}
