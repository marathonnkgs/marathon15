<?php

namespace picof\utils;

class HttpRequest{

	protected $method, $script_name, $request_uri, $query, $get;

	public function __construct(){
		$this->method=$_SERVER['REQUEST_METHOD'];
		$this->script_name=$_SERVER['SCRIPT_NAME'];
		$this->request_uri=$_SERVER['REQUEST_URI'];
		$this->query=$_SERVER['QUERY_STRING'];
		$this->get=$_GET;
	}

	public function __get($attname){
		if(property_exists($this, $attname)){
			return $this->$attname;
		} else {
			throw new \Exception("invalid property");
		}
	}

	public function __set($attname, $attrval){
		if(property_exists($this, $attname)){
			$this->$attname=$attrval;
			return $this->$attname;
		} else throw new \Exception("invalid property");
	}

	public function getPathInfo(){
		$a = str_replace(dirname($this->script_name),'', $this->request_uri);
		if (strpos($a,'?')===false){
			return $a;
		} else{
			$pos = strpos($a,'?');
			return str_replace(substr($a,$pos),'',$a);
		}
		
	}

}