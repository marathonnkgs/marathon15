<?php

namespace picof\auth;

use \PasswordPolicy\Policy;
use picof\auth\AuthException;
use \PasswordLib\PasswordLib;
use Illuminate\Support\Facades\Password;
use blogapp\model\Users;

/**
 * Classe g�rant les authentification
 *        
 */
class Authentication {
	
	public static $profil;
	
	/**
	 * Methode créant un utilisateur
	 * @param string $userName le pseudo fourni par le client
	 * @param string $password le mot de passe fourni par le client
	 * @throws AuthException leve une exception si les données sont invalide
	 */
	public static function createUser($userName, $password) {
		// verifier la police du mot de passe
		$policy = new Policy ();
		$policy->contains('lowercase',$policy->atLeast ( 4 ) );
		$result = $policy->test ( $password );
		if ($result->result == false) {
			throw new AuthException ( "Mauvais mot de passe" );
		}
		// hachage du mot de passe
		$lib = new PasswordLib ();
		$passhash = $lib->createPasswordHash ( $password, '$2a$', array (
				'cost' => 12 
		) );
		// creer et enregistrer l'utlisateur
		$user = new Users ();
		$userclean = filter_var ( $userName, FILTER_SANITIZE_STRING );
		if ($userName != $userclean) {
			throw new AuthException ( "Votre pseudo est invalide" );
		}
		$userExist = Users::where('username','=',$userName)->get();
		if(isset($userExist[0])){
			throw new AuthException("Ce pseudo existe déjà");
		}
		$user->username = $userName;
		$user->pass = $passhash;
		$user->save();
	}
	
	/**
	 * Methode permettant d'authentifier un utlisateur
	 * @param string $username
	 * @param string $password
	 */
	public static function authenticate($username, $password) {
		$userExist = Users::where('username','=',$username)->get();
		if(!isset($userExist[0])){
			throw new AuthException("Ce compte n'existe pas");
		}
		$user = $userExist[0];
		$crypt = new PasswordLib();
		if(! $crypt->verifyPasswordHash($password, $user->pass)){
			throw new AuthException("Mot de passe incorrect");
		}
		self::loadProfile($user->id);
	}
	
	
	private static function loadProfile($uid) {
		$user = Users::find($uid);
		session_destroy();
		session_start();
		$_SESSION['profile']= array(
				'username' => $user->username,
				'userid' => $user->id,
				'client_ip' => $_SERVER["REMOTE_ADDR"]
		);
	}

}