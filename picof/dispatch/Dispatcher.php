<?php

namespace picof\dispatch;

use picof\utils\HttpRequest as hr;

class Dispatcher {
	protected $requete, $routes, $controler;
	public function __construct(hr $httprequest) {
		$this->requete = $httprequest;
		$this->routes = array ();
	}
	
	/**
	 * Permet d'enregistrer une route
	 * 
	 * @param $route l'url        	
	 * @param $controller le
	 *        	nom de la classe controleur
	 * @param $method le
	 *        	nom de la methode a appeler
	 */
	public function addRoute($route, $controller, $method) {
		$this->routes [$route] = array (
				'c' => $controller,
				'm' => $method 
		);
	}
	
	/**
	 * Permet de rediriger les urls
	 */
	public function dispatch() {
		$nomControleur;
		$nomMethode;
		foreach ( $this->routes as $key => $value ) {
			if ($key === $this->requete->getPathInfo ()) {
				$nomControleur = $value ['c'];
				$nomMethode = $value ['m'];
			}
		}
		if ( isset ( $nomControleur )) {
			$this->controler = new $nomControleur( $this->requete );
			return $this->controler->$nomMethode ();
		} else {
			header('Location: http://'.$_SERVER['HTTP_HOST']."".dirname($this->requete->script_name).'/site/accueil');
		}
	}
}